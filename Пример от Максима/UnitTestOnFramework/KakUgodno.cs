﻿using NUnit.Framework;
using NUnitTestOnFramework.API.Requests;
using NUnitTestOnFramework.POM.Methods;
using NUnitTestOnFramework.Support;
using System.Collections.Generic;
using System.Threading;

namespace UnitTestOnFramework
{
    public class KakUgodno : Hooks
    {
        [Test]
        public void PasswordRequiredFramework()
        {
            MainPage.ClickLoginBtn();
            LoginPage.ClickLoginWithEpicBtn();
            LoginPage.ClickPassword();
            LoginPage.ClickEmail();
            var actualError = LoginPage.GetPasswordError();

            Assert.AreEqual("Обязательно", actualError);
        }

        [TestCase("tomate8886@omibrown.com", "C/9;<*eUMSUTDR+")]
        public void SuccessLoginFramework(string email, string password)
        {
            MainPage.ClickLoginBtn();
            LoginPage.ClickLoginWithEpicBtn();
            LoginPage.SetEmail(email);
            LoginPage.SetPassword(password);
            LoginPage.ClickEnterBtn();

            Thread.Sleep(5000);
        }

        [TestCase("nkocura3@gmail.com", "Nata_1234", "123123213")]
        public void SuccessLoginApiFramework(string email, string password, string industry)
        {
            var parameters = new Dictionary<string, object>
            {
                {"industry",industry},
                {"location_name","2343 S Throop St, Chicago, IL 60608, USA"},
                {"location_latitude", "41.8494987"},
                {"location_longitude","-87.6582469"},
                {"location_city_name","Chicago"},
                {"location_admin1_code","IL"},
                {"location_timezone","America/Chicago"}
            };

            var token = Auth.AuthPost(email, password).Token_Data.token;
            Profile.ProfilePatch(token, parameters);
            var actIndustry = Self.SelfGet(token).Client_Profile.industry;

            Assert.AreEqual(industry, actIndustry);
        }
    }
}
