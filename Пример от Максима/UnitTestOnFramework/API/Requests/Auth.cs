﻿using System.Collections.Generic;
using Newtonsoft.Json;
using NUnitTestOnFramework.API.Models;
using NUnitTestOnFramework.Support;
using RestSharp;

namespace NUnitTestOnFramework.API.Requests
{
    internal static class Auth
    {
        internal static AuthModel AuthPost(string email, string password)
        {
            ApiRequest.ConfigureApiRequest("https://api.newbookmodels.com/api/v1/auth/signin/");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"email", email},
                {"password", password}
            };
            ApiRequest.SetData(parameters);

            var response = ApiRequest.SendRequest();
            return JsonConvert.DeserializeObject<AuthModel>(response.Content);
        }
    }
}
