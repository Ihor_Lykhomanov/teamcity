﻿using NUnit.Framework;

namespace NUnitTestOnFramework.Support
{
    public class Hooks
    {
        [SetUp]
        public void SetUp()
        {
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://www.epicgames.com/store/ru/");
        }

        [TearDown]
        public void TearDown()
        {
            ChromeBrowser.CleanDriver();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            ChromeBrowser.CloseDriver();
        }
    }
}