﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cookie = OpenQA.Selenium.Cookie;

namespace NUnitTestOnFramework.Support
{
    internal static class ApiRequest
    {
        private static RestClient _client;
        private static RestRequest _request;

        /// <summary>
        /// Configure Api Request by adding URL and cookies.
        /// </summary>
        /// <param name="url">Base domain + query parameters.</param>
        /// <param name="cookie"></param>
        internal static void ConfigureApiRequest(string url, CookieContainer cookie = null)
        {
            _client = new RestClient(url)
            {
                CookieContainer = cookie,
                Timeout = 60000
            };
        }

        internal static void SetRequestType(Method method)
        {
            _request = new RestRequest(method);
        }

        internal static void SetData(Dictionary<string, object> parameters = null, Dictionary<string, string> headers = null)
        {
            if (headers == null)
                headers = new Dictionary<string, string> { { "content-type", "application/json" } };
            _request.AddHeaders(headers);

            if (parameters != null)
                if (headers.ContainsValue("application/json"))
                {
                    _request.AddJsonBody(parameters);
                    _request.RequestFormat = DataFormat.Json;
                }
                else
                    foreach (var parameter in parameters)
                        _request.AddParameter(parameter.Key, parameter.Value);
        }

        internal static IRestResponse SendRequest()
        {
            var response = _client.Execute(_request);
            response.AnalyzeResponse();

            return response;
        }

        internal static Cookie ExtractCookies(this IRestResponse response, string cookieName)
        {
            foreach (var cookie in response.Cookies)
                if (cookie.Name.Equals(cookieName))
                    return new Cookie(cookie.Name, cookie.Value, cookie.Domain, cookie.Path, null);

            throw new KeyNotFoundException($"Cookie with name {cookieName} not found in api response {response.ResponseUri} ☺");
        }

        private static readonly HttpStatusCode[] _positiveResponse =
        {
            HttpStatusCode.OK, HttpStatusCode.Created, HttpStatusCode.Accepted,
            HttpStatusCode.PartialContent, HttpStatusCode.NonAuthoritativeInformation,
            HttpStatusCode.NoContent, HttpStatusCode.ResetContent
        };

        private static void AnalyzeResponse(this IRestResponse response)
        {
            if (_positiveResponse.Contains(response.StatusCode))
                return;

            throw new Exception(response.Content);
        }
    }
}