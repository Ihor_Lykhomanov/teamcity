﻿using NUnitTestOnFramework.POM.Locators;
using NUnitTestOnFramework.Support;

namespace NUnitTestOnFramework.POM.Methods
{
    internal static class LoginPage
    {
        /// <summary>
        /// Choose login type "Login via Epic Store".
        /// </summary>
        internal static void ClickLoginWithEpicBtn() =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.LoginWithEpicBtn).Click();

        internal static void SetEmail(string email) =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.EmailField).SendKeys(email);

        internal static void SetPassword(string password) =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.PasswordField).SendKeys(password);

        internal static void ClickEmail() =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.EmailField).Click();

        internal static void ClickPassword() =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.PasswordField).Click();
        
        internal static string GetPasswordError() =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.PasswordError).Text;
        
        internal static void ClickEnterBtn() =>
            ChromeBrowser.GetDriver().FindElement(LoginPageL.EnterBtn).Click();
    }
}