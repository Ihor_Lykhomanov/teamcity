@echo ON
if not defined listOfCategories set listOfCategories="cat==success || (cat==fail && cat==bug)"
if not defined projectName set projectName=UnitTestOnFramework

packages\NUnit.ConsoleRunner.3.8.0\tools\nunit3-console.exe ^
%projectName%\bin\Debug\%projectName%.dll --params:param1=tests;param2=%projectName% --where %listOfCategories% ^
--result=result.xml;format=nunit2

packages\SpecFlow.2.4.0\tools\specflow.exe nunitexecutionreport ^
--ProjectFile %projectName%\%projectName%.csproj ^
--xmlTestResult result.xml --testOutput Report.txt --OutputFile CustomSpecflowTestReport.html