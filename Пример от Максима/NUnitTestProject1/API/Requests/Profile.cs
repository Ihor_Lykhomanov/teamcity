﻿using NUnitTestProject1.Support;
using RestSharp;
using System.Collections.Generic;

namespace NUnitTestProject1.API.Requests
{
    internal static class Profile
    {
        internal static IRestResponse ProfilePatch(string token, Dictionary<string, object> parameters)
        {
            ApiRequest.ConfigureApiRequest("https://api.newbookmodels.com/api/v1/client/profile/");
            ApiRequest.SetRequestType(Method.PATCH);

            var headers = new Dictionary<string, string>
            {
                {"authorization", token}
            };
            ApiRequest.SetData(parameters, headers);

            return ApiRequest.SendRequest();
        }
    }
}
