﻿using NUnitTestProject1.POM.Locators;
using NUnitTestProject1.Support;

namespace NUnitTestProject1.POM.Methods
{
    internal static class MainPage
    {
        internal static void ClickLoginBtn() =>
            ChromeBrowser.GetDriver().FindElement(MainPageL.LoginBtn).Click();
    }
}