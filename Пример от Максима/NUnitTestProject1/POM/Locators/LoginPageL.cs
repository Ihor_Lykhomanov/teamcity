﻿using OpenQA.Selenium;

namespace NUnitTestProject1.POM.Locators
{
    internal static class LoginPageL
    {
        internal static By LoginWithEpicBtn = By.Id("login-with-epic");

        internal static By EmailField = By.Id("email");
        internal static By PasswordField = By.Id("password");
        internal static By EnterBtn = By.Id("sign-in");

        internal static By PasswordError = By.CssSelector("#password-helper-text > span");
    }
}